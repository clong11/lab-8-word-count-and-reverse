
#include <stdio.h>
#include <string.h>
#include <ctype.h>

//takes filename as first argument, and returns the number of lines, words, and characters in that file
int main(int argc, char *argv[]) {
    FILE * myFile;
    char *filename = argv[1];
    int lineCount=  0;
    int charCount = 0;
    int wordCount = 0;
    int wordFlag =0;
    printf("Opening file called %s..\n",filename);
    myFile = fopen(filename,"r");
    
    int bufferLength = 1000;
    char buffer[bufferLength];
    
    //run through each line of the file
    while(fgets(buffer, bufferLength, myFile)) {
        //count characters
        charCount += strlen(buffer);
        //count words
        for(int i =0;i<strlen(buffer);i++){
            //only count word at the beginning of the word
            if(isalnum(buffer[i]) && wordFlag == 0){
                wordFlag = 1;
                wordCount++;
            }
            //start looking for next word when it finds a non alphanumeric letter
            else if(!isalnum(buffer[i]) && wordFlag ==1){
                wordFlag = 0;
            }
        }
        //count lines
        lineCount++;
    }

    fclose(myFile);
    printf("%s has \n",filename);
    printf("lines: %d \ncharacters: %d\nwords: %d",lineCount,charCount,wordCount);
    return 0; 

}

