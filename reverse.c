
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

//takes filename as first argument, output filename as the second argument
int main(int argc, char *argv[]) {
    FILE * inFile;
    FILE * outFile;
    char *inFileName = argv[1];
    char *outFileName = argv[2];
    if(inFileName == NULL || outFileName == NULL){
        printf("input file or output file not specified. exiting..\n");
        exit(0);
    }
    // int lineCount=  0;
    // int charCount = 0;
    // int wordCount = 0;
    // int wordFlag =0;
    //printf("Opening file called %s..\n",inFile);
    inFile = fopen(inFileName,"r");
    outFile = fopen(outFileName,"w");
    
    int bufferLength = 255;
    char reverseBuffer[bufferLength];
    char buffer[bufferLength];
    
    //run through each line of the file
    while(fgets(buffer, bufferLength, inFile)) {
        
        printf("%s",buffer);
        for(int i =0;i<strlen(buffer);i++){
            reverseBuffer[bufferLength-i-1]=buffer[i];
        }
        //printf("reversed: %s",reverseBuffer);
        
        printf("%s\n",&reverseBuffer[bufferLength-strlen(buffer)]);
        fwrite(&reverseBuffer[bufferLength-strlen(buffer)],1,strlen(buffer),outFile);
        fwrite("\n",1,1,outFile);
        memset(reverseBuffer, 0, sizeof(reverseBuffer));
        
    }
        printf("\n");

    fclose(inFile);
    return 0; 

}

